# README #

Kayra is a Content Management System (CMS) intended for use by
[EVE Online](http://www.eveonline.com) corporations and alliances.  It is still
in an early pre-alpha stage of development and is not yet ready for use in
production environments.

## Documentation ##

All user and developer documentation is available from our
[wiki site](https://bitbucket.org/makhar/kayra/wiki/).

## Contributors ##

* [Makhar](https://gate.eveonline.com/Profile/Makhar)
