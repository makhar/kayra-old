<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.3/css/bootstrap.min.css" integrity="sha384-MIwDKRSSImVFAZCVLtU0LMDdON6KVCrZHyVQQj6e8wIEJkW4tvwqXrbMIya1vriY" crossorigin="anonymous">
	<title>Website | {% block title %}{% endblock %}</title>
</head>
<body>
	{% include 'templates/partials/messages.php' %}
	{% include 'templates/partials/navigation.php' %}
	{% block content %}{% endblock %}
	{% include 'templates/partials/scripts.php' %}
</body>
</html>
